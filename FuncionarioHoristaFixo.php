<?php

require_once("./FuncionarioHorista.php");
require_once("./Endereco.php");

class FuncionarioHoristaFixo extends FuncionarioHorista
{
  protected int $horasFixas;
  protected int $porcentagemHorasExtras;

  public function __construct(string $nome, Endereco $endereco, string $cpf, int $inss, float $valorHora, float $horasTrabalhadas, int $porcentagemHorasExtras, ?int $horasFixas)
  {
    parent::__construct($nome, $endereco, $cpf, $inss, $valorHora, $horasTrabalhadas);

    if ($porcentagemHorasExtras <= 0) {
      $this->porcentagemHorasExtras = 0;
    } else {
      $this->porcentagemHorasExtras = $porcentagemHorasExtras;
    }

    if (isset($horasFixas)) {
      $this->horasFixas = $horasFixas;
    } else {
      $this->horasFixas = 160;
    }
  }

  public function getHorasFixas()
  {
    return $this->horasFixas;
  }
  public function getPorcentagemHorasExtras()
  {
    return $this->porcentagemHorasExtras;
  }

  public function setHorasFixas(int $horasFixas)
  {
    if ($horasFixas <= 0) {
      throw new Exception("Horas fixas devem ser maior que zero.");
    }
    $this->horasFixas = $horasFixas;
  }
  public function setPorcentagemHorasExtras(int $porcentagemHorasExtras)
  {
    if ($porcentagemHorasExtras <= 0) {
      throw new Exception("Porcentagem de horas extras devem ser maior que zero.");
    }
    $this->porcentagemHorasExtras = $porcentagemHorasExtras;
  }

  public function getSalario()
  {
    if ($this->horasTrabalhadas > $this->horasFixas) {
      // Horas extras
      return $this->valorHora * $this->horasFixas + ($this->valorHora * $this->porcentagemHorasExtras / 100) * ($this->horasTrabalhadas - $this->horasFixas);
    } else {
      // Horas fixas
      parent::getSalario();
    }
  }
}
