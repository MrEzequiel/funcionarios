<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;900&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="./style/home.css">
</head>

<body>
  <div class="box"></div>

  <header>
    <h1>Funcionário</h1>
    <p>Cadastrar funcionários!</p>
  </header>

  <div class="form-wrapper">
    <div class="tab">
      <button class="tablinks" onclick="openTab(event, 'comissionado')">Comissionado</button>
      <button class="tablinks" onclick="openTab(event, 'comissionado-piso')">Comissionado Piso</button>
      <button class="tablinks" onclick="openTab(event, 'fixo')">Fixo</button>
      <button class="tablinks" onclick="openTab(event, 'horista')">Horista</button>
      <button class="tablinks" onclick="openTab(event, 'horista-fixo')">Horista Fixo</button>
    </div>

    <div id="comissionado" class="tabcontent">
      <h3>Comissionado</h3>

      <form method="post" action="./cadastro.php">
        <input type="hidden" value="comissionado" name="type" />

        <fieldset>
          <legend>Informações Pessoais: </legend>
          <div class="group">
            <div class="input-field">
              <label for="nome">Nome</label>
              <input type="text" id="nome" name="nome" required />
            </div>
            <div class="input-field">
              <label for="cpf">CPF</label>
              <input type="number" id="cpf" name="cpf" required />
            </div>
            <div class="input-field">
              <label for="inss">INSS</label>
              <input type="number" id="inss" name="inss" required />
            </div>
          </div>

          <!-- Endereço -->
          <div class="group">
            <div class="input-field">
              <label for="rua">Rua</label>
              <input type="text" id="rua" name="rua" required />
            </div>
            <div class="input-field">
              <label for="bairro">Bairro</label>
              <input type="text" id="bairro" name="bairro" required />
            </div>
            <div class="input-field">
              <label for="cidade">Cidade</label>
              <input type="text" id="cidade" name="cidade" required />
            </div>
            <div class="input-field">
              <label for="estado">Estado</label>
              <input type="text" id="estado" name="estado" required />
            </div>
            <div class="input-field">
              <label for="numResidencia">Número da residência</label>
              <input type="number" id="numResidencia" name="numResidencia" required />
            </div>
          </div>
        </fieldset>


        <fieldset>
          <legend>Informações Funcionário: </legend>
          <div class="group">
            <div class="input-field">
              <label for="valorVenda">Valor da venda</label>
              <input type="number" id="valorVenda" name="valorVenda" required />
            </div>
            <div class="input-field">
              <label for="comissao">Comissão</label>
              <input type="number" id="comissao" name="comissao" required />
            </div>
          </div>
        </fieldset>

        <div class="button-wrapper">
          <button type="submit">
            Cadastrar
          </button>
        </div>
      </form>
    </div>

    <div id="comissionado-piso" class="tabcontent">
      <h3>Comissionado Piso</h3>

      <form method="post" action="./cadastro.php">
        <input type="hidden" value="comissionado-piso" name="type" />

        <fieldset>
          <legend>Informações Pessoais: </legend>
          <div class="group">
            <div class="input-field">
              <label for="nome">Nome</label>
              <input type="text" id="nome" name="nome" required />
            </div>
            <div class="input-field">
              <label for="cpf">CPF</label>
              <input type="number" id="cpf" name="cpf" required />
            </div>
            <div class="input-field">
              <label for="inss">INSS</label>
              <input type="number" id="inss" name="inss" required />
            </div>
          </div>

          <!-- Endereço -->
          <div class="group">
            <div class="input-field">
              <label for="rua">Rua</label>
              <input type="text" id="rua" name="rua" required />
            </div>
            <div class="input-field">
              <label for="bairro">Bairro</label>
              <input type="text" id="bairro" name="bairro" required />
            </div>
            <div class="input-field">
              <label for="cidade">Cidade</label>
              <input type="text" id="cidade" name="cidade" required />
            </div>
            <div class="input-field">
              <label for="estado">Estado</label>
              <input type="text" id="estado" name="estado" required />
            </div>
            <div class="input-field">
              <label for="numResidencia">Número da residência</label>
              <input type="number" id="numResidencia" name="numResidencia" required />
            </div>
          </div>
        </fieldset>


        <fieldset>
          <legend>Informações Funcionário: </legend>
          <div class="group">
            <div class="input-field">
              <label for="valorVenda">Valor da venda</label>
              <input type="number" id="valorVenda" name="valorVenda" required />
            </div>
            <div class="input-field">
              <label for="comissao">Comissão</label>
              <input type="number" id="comissao" name="comissao" required />
            </div>
            <div class="input-field">
              <label for="pisoSalarial">Piso salarial</label>
              <input type="number" id="pisoSalarial" name="pisoSalarial" required />
            </div>
          </div>
        </fieldset>

        <div class="button-wrapper">
          <button type="submit">
            Cadastrar
          </button>
        </div>
      </form>
    </div>

    <div id="fixo" class="tabcontent">
      <h3>Fixo</h3>

      <form method="post" action="./cadastro.php">
        <input type="hidden" value="fixo" name="type" />

        <fieldset>
          <legend>Informações Pessoais: </legend>
          <div class="group">
            <div class="input-field">
              <label for="nome">Nome</label>
              <input type="text" id="nome" name="nome" required />
            </div>
            <div class="input-field">
              <label for="cpf">CPF</label>
              <input type="number" id="cpf" name="cpf" required />
            </div>
            <div class="input-field">
              <label for="inss">INSS</label>
              <input type="number" id="inss" name="inss" required />
            </div>
          </div>

          <!-- Endereço -->
          <div class="group">
            <div class="input-field">
              <label for="rua">Rua</label>
              <input type="text" id="rua" name="rua" required />
            </div>
            <div class="input-field">
              <label for="bairro">Bairro</label>
              <input type="text" id="bairro" name="bairro" required />
            </div>
            <div class="input-field">
              <label for="cidade">Cidade</label>
              <input type="text" id="cidade" name="cidade" required />
            </div>
            <div class="input-field">
              <label for="estado">Estado</label>
              <input type="text" id="estado" name="estado" required />
            </div>
            <div class="input-field">
              <label for="numResidencia">Número da residência</label>
              <input type="number" id="numResidencia" name="numResidencia" required />
            </div>
          </div>
        </fieldset>


        <fieldset>
          <legend>Informações Funcionário: </legend>
          <div class="group">
            <div class="input-field">
              <label for="salario">Salário</label>
              <input type="number" id="salario" name="salario" required />
            </div>
          </div>
        </fieldset>

        <div class="button-wrapper">
          <button type="submit">
            Cadastrar
          </button>
        </div>
      </form>
    </div>

    <div id="horista" class="tabcontent">
      <h3>Horista</h3>

      <form method="post" action="./cadastro.php">
        <input type="hidden" value="horista" name="type" />
        <fieldset>
          <legend>Informações Pessoais: </legend>
          <div class="group">
            <div class="input-field">
              <label for="nome">Nome</label>
              <input type="text" id="nome" name="nome" required />
            </div>
            <div class="input-field">
              <label for="cpf">CPF</label>
              <input type="number" id="cpf" name="cpf" required />
            </div>
            <div class="input-field">
              <label for="inss">INSS</label>
              <input type="number" id="inss" name="inss" required />
            </div>
          </div>

          <!-- Endereço -->
          <div class="group">
            <div class="input-field">
              <label for="rua">Rua</label>
              <input type="text" id="rua" name="rua" required />
            </div>
            <div class="input-field">
              <label for="bairro">Bairro</label>
              <input type="text" id="bairro" name="bairro" required />
            </div>
            <div class="input-field">
              <label for="cidade">Cidade</label>
              <input type="text" id="cidade" name="cidade" required />
            </div>
            <div class="input-field">
              <label for="estado">Estado</label>
              <input type="text" id="estado" name="estado" required />
            </div>
            <div class="input-field">
              <label for="numResidencia">Número da residência</label>
              <input type="number" id="numResidencia" name="numResidencia" required />
            </div>
          </div>
        </fieldset>


        <fieldset>
          <legend>Informações Funcionário: </legend>
          <div class="group">
            <div class="input-field">
              <label for="valorHora">Valor hora</label>
              <input type="number" name="valorHora" id="valorHora" required step="0.01" />
            </div>
            <div class="input-field">
              <label for="horasTrabalhadas">Horas trabalhadas</label>
              <input type="number" name="horasTrabalhadas" id="horasTrabalhadas" required step="1" />
            </div>
          </div>
        </fieldset>

        <div class="button-wrapper">
          <button type="submit">
            Cadastrar
          </button>
        </div>
      </form>
    </div>

    <div id="horista-fixo" class="tabcontent">
      <h3>Horista Fixo</h3>

      <form method="post" action="./cadastro.php">
        <input type="hidden" value="horista-fixo" name="type" />
        <fieldset>
          <legend>Informações Pessoais: </legend>
          <div class="group">
            <div class="input-field">
              <label for="nome">Nome</label>
              <input type="text" id="nome" name="nome" required />
            </div>
            <div class="input-field">
              <label for="cpf">CPF</label>
              <input type="number" id="cpf" name="cpf" required />
            </div>
            <div class="input-field">
              <label for="inss">INSS</label>
              <input type="number" id="inss" name="inss" required />
            </div>
          </div>

          <!-- Endereço -->
          <div class="group">
            <div class="input-field">
              <label for="rua">Rua</label>
              <input type="text" id="rua" name="rua" required />
            </div>
            <div class="input-field">
              <label for="bairro">Bairro</label>
              <input type="text" id="bairro" name="bairro" required />
            </div>
            <div class="input-field">
              <label for="cidade">Cidade</label>
              <input type="text" id="cidade" name="cidade" required />
            </div>
            <div class="input-field">
              <label for="estado">Estado</label>
              <input type="text" id="estado" name="estado" required />
            </div>
            <div class="input-field">
              <label for="numResidencia">Número da residência</label>
              <input type="number" id="numResidencia" name="numResidencia" required />
            </div>
          </div>
        </fieldset>


        <fieldset>
          <legend>Informações Funcionário: </legend>
          <div class="group">
            <div class="input-field">
              <label for="valorHora">Valor hora</label>
              <input type="number" name="valorHora" id="valorHora" required step="0.01" />
            </div>
            <div class="input-field">
              <label for="horasTrabalhadas">Horas trabalhadas</label>
              <input type="number" name="horasTrabalhadas" id="horasTrabalhadas" required step="1" />
            </div>
            <div class="input-field">
              <label for="porcentagemHorasExtras">Porcentagem de horas extras</label>
              <input type="number" name="porcentagemHorasExtras" id="porcentagemHorasExtras" required step="1" />
            </div>
            <div class="input-field">
              <label for="horasFixas">Horas fixas</label>
              <input type="number" name="horasFixas" id="horasFixas" step="1" />
            </div>
          </div>
        </fieldset>

        <div class="button-wrapper">
          <button type="submit">
            Cadastrar
          </button>
        </div>
      </form>
    </div>
  </div>

  <script>
    function openTab(evt, tabName) {
      let i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(tabName).style.display = "block";
      evt.currentTarget.className += " active";
    }

    const tabLink = document.querySelector(".tablinks")
    tabLink.click()
  </script>

</body>

</html>