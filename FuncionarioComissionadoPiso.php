<?php

require_once("./FuncionarioComissionado.php");
require_once("./Endereco.php");

class FuncionarioComissionadoPiso extends FuncionarioComissionado
{
  protected float $pisoSalarial;

  public function __construct(string $nome, Endereco $endereco, string $cpf, int $inss, float $valorVenda, float $comissao, float $pisoSalarial)
  {
    parent::__construct($nome, $endereco, $cpf, $inss, $valorVenda, $comissao);
    if ($pisoSalarial <= 0) {
      $this->pisoSalarial = 0.0;
    } else {
      $this->pisoSalarial = $pisoSalarial;
    }
  }

  public function getPisoSalarial()
  {
    return $this->pisoSalarial;
  }

  public function setPisoSalarial(float $pisoSalarial)
  {
    if ($pisoSalarial <= 0) {
      throw new Exception("Piso salarial deve ser maior que zero.");
    }
    $this->pisoSalarial = $pisoSalarial;
  }

  public function getSalario()
  {
    return $this->getPisoSalarial() + parent::getSalario();
  }
}
