<?php

require_once("./Funcionario.php");
require_once("./Endereco.php");

class FuncionarioComissionado extends Funcionario
{
  protected float $valorVenda;
  protected float $comissao; // comissão por venda em %. ex. 5, 10, etc.

  public function __construct(string $nome, Endereco $endereco, string $cpf, int $inss, float $valorVenda, float $comissao)
  {
    parent::__construct($nome, $endereco, $cpf, $inss);

    if ($valorVenda <= 0) {
      $this->valorVenda = 0.0;
    } else {
      $this->valorVenda = $valorVenda;
    }

    if ($comissao <= 0) {
      $this->comissao = 0.0;
    } else {
      $this->comissao = $comissao;
    }
  }

  public function getValorVenda()
  {
    return $this->valorVenda;
  }
  public function getComissao()
  {
    return $this->comissao;
  }

  public function setValorVenda(float $valorVenda)
  {
    if ($valorVenda <= 0) {
      throw new Exception("Valor da venda deve ser maior que zero.");
    }
    $this->valorVenda = $valorVenda;
  }
  public function setComissao(float $comissao)
  {
    if ($comissao <= 0) {
      throw new Exception("Comissão deve ser maior que zero.");
    }
    $this->comissao = $comissao;
  }

  public function getSalario()
  {
    return $this->valorVenda * ($this->comissao / 100);
  }

  public function __toString()
  {
    return parent::__toString() . "Salario: {$this->getSalario()}\n";
  }
}
