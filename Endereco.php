<?php
class Endereco
{
  protected int $numResidencia;
  protected string $rua;
  protected string $bairro;
  protected string $cidade;
  protected string $estado;

  public function __construct(int $numResidencia, string $rua, string $bairro, string $cidade, string $estado)
  {
    $this->numResidencia = $numResidencia;
    $this->rua = $rua;
    $this->bairro = $bairro;
    $this->cidade = $cidade;
    $this->estado = $estado;
  }

  public function getNumResidencia()
  {
    return $this->numResidencia;
  }
  public function getRua()
  {
    return $this->rua;
  }
  public function getBairro()
  {
    return $this->bairro;
  }
  public function getCidade()
  {
    return $this->cidade;
  }
  public function getEstado()
  {
    return $this->estado;
  }

  public function __toString()
  {
    return "Endereco: {$this->numResidencia}, {$this->rua}, {$this->bairro}, {$this->cidade}, {$this->estado}";
  }
}
