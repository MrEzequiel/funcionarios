<?php
require_once('./Endereco.php');

abstract class Funcionario
{
  protected string $nome;
  protected Endereco $endereco;
  protected string $cpf;
  protected int $inss;

  public function __construct(string $nome, Endereco $endereco, string $cpf, int $inss)
  {
    $this->nome = $nome;
    $this->endereco = $endereco;
    $this->cpf = $cpf;
    $this->inss = $inss;
  }

  public function getNome()
  {
    return $this->nome;
  }
  public function getEndereco()
  {
    return $this->endereco;
  }
  public function getCpf()
  {
    return $this->cpf;
  }
  public function getInss()
  {
    return $this->inss;
  }

  public function setNome(string $nome)
  {
    $this->nome = $nome;
  }
  public function setEndereco(Endereco $endereco)
  {
    $this->endereco = $endereco;
  }
  public function setCpf(string $cpf)
  {
    $this->cpf = $cpf;
  }
  public function setInss(int $inss)
  {
    $this->inss = $inss;
  }

  abstract public function getSalario();

  public function __toString()
  {
    return "== Funcionario ==\nNome: {$this->nome}\n{$this->endereco}\nCPF: {$this->cpf}\nINSS: {$this->inss}\n";
  }
}
