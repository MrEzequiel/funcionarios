<?php

require_once("./Funcionario.php");
require_once("./Endereco.php");

class FuncionarioFixo extends Funcionario
{
  protected float $salario;

  public function __construct(string $nome, Endereco $endereco, string $cpf, int $inss, float $salario)
  {
    parent::__construct($nome, $endereco, $cpf, $inss);
    $this->salario = $salario;
  }

  public function setSalario(float $salario)
  {
    $this->salario = $salario <= 0 ? 0.0 : $salario;
  }

  public function getSalario()
  {
    return $this->salario;
  }

  public function __toString()
  {
    return parent::__toString() . "Salario: {$this->salario}\n";
  }
}
