<?php

require_once("./Funcionario.php");
require_once("./Endereco.php");

class FuncionarioHorista extends Funcionario
{
  protected float $valorHora;
  protected float $horasTrabalhadas;

  public function __construct(string $nome, Endereco $endereco, string $cpf, int $inss, float $valorHora, float $horasTrabalhadas)
  {
    parent::__construct($nome, $endereco, $cpf, $inss);

    if ($valorHora <= 0) {
      $this->valorHora = 0.0;
    } else {
      $this->valorHora = $valorHora;
    }

    if ($horasTrabalhadas <= 0) {
      $this->horasTrabalhadas = 0.0;
    } else {
      $this->horasTrabalhadas = $horasTrabalhadas;
    }
  }

  public function getValorHora()
  {
    return $this->valorHora;
  }
  public function getHorasTrabalhadas()
  {
    return $this->horasTrabalhadas;
  }

  public function setValorHora(float $valorHora)
  {
    if ($valorHora <= 0) {
      throw new Exception("Valor da hora deve ser maior que zero.");
    }
    $this->valorHora = $valorHora;
  }
  public function setHorasTrabalhadas(float $horasTrabalhadas)
  {
    if ($horasTrabalhadas <= 0) {
      throw new Exception("Horas trabalhadas devem ser maior que zero.");
    }
    $this->horasTrabalhadas = $horasTrabalhadas;
  }

  public function getSalario()
  {
    return $this->valorHora * $this->horasTrabalhadas;
  }

  public function __toString()
  {
    return parent::__toString() . "Salario: {$this->getSalario()}\n";
  }
}
