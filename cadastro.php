<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./style/cadastro.css">
  <title>Cadastro</title>
</head>

<body>
  <div class="box"></div>
  <?php

  require_once("./Endereco.php");
  require_once("./Funcionario.php");
  require_once("./FuncionarioComissionado.php");
  require_once("./FuncionarioComissionadoPiso.php");
  require_once("./FuncionarioFixo.php");
  require_once("./FuncionarioHorista.php");
  require_once("./FuncionarioHoristaFixo.php");

  $typeFunc = $_POST['type'];

  function pegarInformacoesBasicas()
  {
    return array(
      "nome" => $_POST['nome'],
      "cpf" => $_POST['cpf'],
      "inss" => $_POST['inss'],
      "endereco" => array(
        "rua" => $_POST["rua"],
        "bairro" => $_POST["bairro"],
        "cidade" => $_POST["cidade"],
        "estado" => $_POST["estado"],
        "numResidencia" => $_POST["numResidencia"]
      ),
    );
  }

  function pegarInstanciaEndereco($endereco): Endereco
  {
    return new Endereco(
      $endereco["numResidencia"],
      $endereco["rua"],
      $endereco["bairro"],
      $endereco["cidade"],
      $endereco["estado"]
    );
  }

  function cadastroFuncionarioComissionado()
  {
    $informacoes = pegarInformacoesBasicas();
    $endereco = pegarInstanciaEndereco($informacoes["endereco"]);

    try {
      echo "<h2>Funcionário cadastrado!</h2>";
      $funcionario = new FuncionarioComissionado(
        $_POST["nome"],
        $endereco,
        $_POST['cpf'],
        $_POST["inss"],
        $_POST['valorVenda'],
        $_POST["comissao"],
      );

      echo "
        <ul>
          <li><strong>Nome:</strong> {$funcionario->getNome()}</li>
          <li><strong>Endereço:</strong> {$funcionario->getEndereco()->__toString()}</li>
          <li><strong>CPF:</strong> {$funcionario->getCpf()}</li>
          <li><strong>INSS:</strong> {$funcionario->getInss()}</li>
          <li><strong>Salário:</strong> {$funcionario->getSalario()}</li>
        </ul>
      ";
    } catch (Exception $e) {
      echo "<h3>Ocorreu um error no cadastro de funcionário</h3>";
    }
  }

  function cadastroFuncionarioComissionadoPiso()
  {
    $informacoes = pegarInformacoesBasicas();
    $endereco = pegarInstanciaEndereco($informacoes["endereco"]);

    try {
      echo "<h2>Funcionário cadastrado!</h2><br>";
      $funcionario = new FuncionarioComissionadoPiso(
        $_POST["nome"],
        $endereco,
        $_POST['cpf'],
        $_POST["inss"],
        $_POST['valorVenda'],
        $_POST["comissao"],
        $_POST["pisoSalarial"]
      );

      echo "
      <ul>
        <li><strong>Nome:</strong> {$funcionario->getNome()}</li>
        <li><strong>Endereço:</strong> {$funcionario->getEndereco()->__toString()}</li>
        <li><strong>CPF:</strong> {$funcionario->getCpf()}</li>
        <li><strong>INSS:</strong> {$funcionario->getInss()}</li>
        <li><strong>Salário:</strong> {$funcionario->getSalario()}</li>
      </ul>
    ";
    } catch (Exception $e) {
      echo "<h3>Ocorreu um error no cadastro de funcionário</h3>";
    }
  }

  function cadastroFuncionarioFixo()
  {
    $informacoes = pegarInformacoesBasicas();
    $endereco = pegarInstanciaEndereco($informacoes["endereco"]);

    try {
      echo "<h2>Funcionário cadastrado!</h2><br>";
      $funcionario = new FuncionarioFixo(
        $_POST["nome"],
        $endereco,
        $_POST["cpf"],
        $_POST["inss"],
        $_POST["salario"]
      );

      echo "
      <ul>
        <li><strong>Nome:</strong> {$funcionario->getNome()}</li>
        <li><strong>Endereço:</strong> {$funcionario->getEndereco()->__toString()}</li>
        <li><strong>CPF:</strong> {$funcionario->getCpf()}</li>
        <li><strong>INSS:</strong> {$funcionario->getInss()}</li>
        <li><strong>Salário:</strong> {$funcionario->getSalario()}</li>
      </ul>
    ";
    } catch (Exception $e) {
      echo "<h3>Ocorreu um error no cadastro de funcionário</h3>";
    }
  }

  function cadastroFuncionarioHorista()
  {
    $informacoes = pegarInformacoesBasicas();
    $endereco = pegarInstanciaEndereco($informacoes["endereco"]);

    try {
      echo "<h2>Funcionário cadastrado!</h2><br>";

      $funcionario = new FuncionarioHorista(
        $_POST["nome"],
        $endereco,
        $_POST["cpf"],
        $_POST["inss"],
        $_POST["valorHora"],
        $_POST["horasTrabalhadas"]
      );


      echo "
        <ul>
          <li><strong>Nome:</strong> {$funcionario->getNome()}</li>
          <li><strong>Endereço:</strong> {$funcionario->getEndereco()->__toString()}</li>
          <li><strong>CPF:</strong> {$funcionario->getCpf()}</li>
          <li><strong>INSS:</strong> {$funcionario->getInss()}</li>
          <li><strong>Salário:</strong> {$funcionario->getSalario()}</li>
        </ul>
      ";
    } catch (Exception $e) {
      echo "<h3>Ocorreu um error no cadastro de funcionário</h3>";
    }
  }

  function cadastroFuncionarioHoristaFixo()
  {
    $informacoes = pegarInformacoesBasicas();
    $endereco = pegarInstanciaEndereco($informacoes["endereco"]);

    try {
      echo "<h2>Funcionário cadastrado!</h2><br>";
      $funcionario = new FuncionarioHoristaFixo(
        $_POST["nome"],
        $endereco,
        $_POST["cpf"],
        $_POST["inss"],
        $_POST["valorHora"],
        $_POST["horasTrabalhadas"],
        $_POST["porcentagemHorasExtras"],
        $_POST["horasFixas"]
      );

      echo "
        <ul>
          <li><strong>Nome:</strong> {$funcionario->getNome()}</li>
          <li><strong>Endereço:</strong> {$funcionario->getEndereco()->__toString()}</li>
          <li><strong>CPF:</strong> {$funcionario->getCpf()}</li>
          <li><strong>INSS:</strong> {$funcionario->getInss()}</li>
          <li><strong>Salário:</strong> {$funcionario->getSalario()}</li>
        </ul>
      ";
    } catch (Exception $e) {
      echo "<h3>Ocorreu um error no cadastro de funcionário</h3>";
    }
  }


  switch ($typeFunc) {
    case "comissionado":
      cadastroFuncionarioComissionado();
      break;

    case "comissionado-piso":
      cadastroFuncionarioComissionadoPiso();
      break;

    case "fixo":
      cadastroFuncionarioFixo();
      break;

    case "horista":
      cadastroFuncionarioHorista();
      break;

    case "horista-fixo":
      cadastroFuncionarioHoristaFixo();
      break;


    default:
      echo "<h2>Tipo de funcionário não encontrado!</h2>";
  }
  ?>
</body>

</html>